package dao;

import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class UserDaoImplementation implements UserDao {

    SessionFactory sessionFactory;
    public UserDaoImplementation()
    {
        sessionFactory=Util.getSessionFactory();
    }
    public User createUser(User user) {
        Session session = this.sessionFactory.openSession();
        session.save(user);
        session.close();
        return user;
    }

    public User getUser(long id_user) {
        Session session=this.sessionFactory.openSession();
        User retrievedUser = session.get(User.class,1l);
        session.save(retrievedUser);
        session.close();
        return retrievedUser;
    }

    public User updateUser(User user, long id_user) {
        Session session=this.sessionFactory.openSession();
        User retrievedUser=session.get(User.class,id_user);
        retrievedUser.setName(user.getName());
        session.update(retrievedUser);
        Transaction transaction=session.beginTransaction();
        transaction.commit();
        session.close();
        return retrievedUser;
    }

    public void deleteUser(long id_user) {
        Session session=this.sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        User retrievedeUser=session.get(User.class,id_user);
        session.remove(retrievedeUser);
        session.close();

    }
}
