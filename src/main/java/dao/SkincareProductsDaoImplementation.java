package dao;

import model.SkincareProducts;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SkincareProductsDaoImplementation implements SkincareProductsDao {

    SessionFactory sessionFactory;

    public SkincareProductsDaoImplementation(){
        sessionFactory=Util.getSessionFactory();
    }

    public SkincareProducts createSkinCareProduct(SkincareProducts skincareProducts) {
        Session session = this.sessionFactory.openSession();
        session.save(skincareProducts);
        session.close();
        return skincareProducts;
    }

    public SkincareProducts getSkinCareProduct(long id_product) {
        Session session=this.sessionFactory.openSession();
        SkincareProducts retrievedSkincareProduct=session.get(SkincareProducts.class,1l);
        session.save(retrievedSkincareProduct);
        return retrievedSkincareProduct;
    }

    public SkincareProducts updateSkinCareProduct(SkincareProducts skincareProducts, long id_product) {
       Session session=this.sessionFactory.openSession();
       SkincareProducts retrievedSkincareProduct=session.get(SkincareProducts.class,id_product);
       retrievedSkincareProduct.setName(skincareProducts.getName());
       retrievedSkincareProduct.setDescription(skincareProducts.getDescription());
       retrievedSkincareProduct.setSPF(skincareProducts.getSPF());
        return retrievedSkincareProduct;
    }

    public void deleteSkinCareProduct(long id_product) {
        Session session =this.sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        SkincareProducts retrievedSkincareProduct=session.get(SkincareProducts.class,id_product);
        session.remove(retrievedSkincareProduct);
        session.close();


    }
}
