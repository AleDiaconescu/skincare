package dao;

import model.SkinType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SkinTypeDaoImplementation implements SkinTypeDao {

    SessionFactory sessionFactory;
    public SkinTypeDaoImplementation() {

        sessionFactory=Util.getSessionFactory();
    }

    public SkinType createSkinType(SkinType skinType) {
        Session session =this.sessionFactory.openSession();
        session.save(skinType);
        session.close();
        return skinType;
    }

    public SkinType getSkinType(long id_skin) {
        Session session=this.sessionFactory.openSession();
        SkinType retrievedSkinType=session.get(SkinType.class,1l);
        session.save(retrievedSkinType);
        return retrievedSkinType;
    }

    public SkinType updateSkinType(SkinType skinType, long id_skin) {
        Session session=this.sessionFactory.openSession();
        SkinType retrievdSkinType=session.get(SkinType.class,id_skin);
        retrievdSkinType.setName(skinType.getName());
        retrievdSkinType.setCharacteristics(skinType.getCharacteristics());
        session.update(retrievdSkinType);
        Transaction transaction =session.beginTransaction();
        transaction.commit();
        session.close();
        return retrievdSkinType;
    }

    public void deleteSkinType(long id_skin) {
        Session session=this.sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        SkinType retrievedSkinType=session.get(SkinType.class,id_skin);
        session.remove(retrievedSkinType);
        transaction.commit();
        session.close();

    }
}
