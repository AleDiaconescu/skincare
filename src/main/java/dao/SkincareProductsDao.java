package dao;

import model.SkincareProducts;

public interface SkincareProductsDao {
    SkincareProducts createSkinCareProduct(SkincareProducts skincareProducts);
    SkincareProducts getSkinCareProduct(long id_product);
    SkincareProducts updateSkinCareProduct(SkincareProducts skincareProducts,long id_product);
    void deleteSkinCareProduct(long id_product);
}
