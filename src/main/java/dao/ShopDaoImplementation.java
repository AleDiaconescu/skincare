package dao;

import model.Shop;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ShopDaoImplementation implements ShopDao {

    SessionFactory sessionFactory;

    public ShopDaoImplementation (){

        sessionFactory=Util.getSessionFactory();
    }

    public Shop createShop(Shop shop) {
        Session session=this.sessionFactory.openSession();
        session.save(shop);
        session.close();
        return shop;
    }

    public Shop getShop(long id_shop) {
        Session session=this.sessionFactory.openSession();
        Shop retrievedShop=session.get(Shop.class,1l);
        session.save(retrievedShop);
        session.close();
        return retrievedShop;
    }

    public Shop updateShop(Shop shop, long id_shop) {
        Session session=this.sessionFactory.openSession();
        Shop retrievedShop=session.get(Shop.class,id_shop);
        retrievedShop.setAdress(shop.getAdress());
        retrievedShop.setName(shop.getName());
        retrievedShop.setStock(shop.getStock());
        retrievedShop.setPrice(shop.getPrice());
        session.update(retrievedShop);
        Transaction transaction=session.beginTransaction();
        transaction.commit();
        session.close();
        return retrievedShop;

    }

    public void deleteShop(long id_shop) {
        Session session=sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        Shop retrievedShop=session.get(Shop.class,id_shop);
        session.remove(retrievedShop);
        transaction.commit();
        session.close();


    }
}
