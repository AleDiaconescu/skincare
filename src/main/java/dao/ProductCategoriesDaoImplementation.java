package dao;

import model.ProductCategories;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ProductCategoriesDaoImplementation implements ProductCategoriesDao {

    SessionFactory sessionFactory;

    public ProductCategoriesDaoImplementation(){
        sessionFactory=Util.getSessionFactory();
    }
    public ProductCategories createProductCategories(ProductCategories productCategories) {
        Session session=this.sessionFactory.openSession();
        session.save(productCategories);
        session.close();
        return productCategories;
    }

    public ProductCategories getProductCategories(long id_category) {
        Session session =this.sessionFactory.openSession();
        ProductCategories retrievedProductCategories=session.get(ProductCategories.class,1l);
        session.save(retrievedProductCategories);
        session.close();
        return retrievedProductCategories;
    }

    public ProductCategories updateProductCategories(ProductCategories productCategories, long id_category) {
        Session session=this.sessionFactory.openSession();
        ProductCategories retrievedProductCategories=session.get(ProductCategories.class,id_category);
        retrievedProductCategories.setName(productCategories.getName());
        session.update(retrievedProductCategories);
        Transaction transaction=session.beginTransaction();
        transaction.commit();
        session.close();
        return retrievedProductCategories;
    }

    public void deleteProductCategories(long id_category) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        ProductCategories retrievedProductCategories=session.get(ProductCategories.class,id_category);
        session.remove(retrievedProductCategories);
        transaction.commit();

    }
}
