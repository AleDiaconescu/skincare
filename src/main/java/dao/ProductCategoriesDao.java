package dao;

import model.ProductCategories;

public interface ProductCategoriesDao {
    ProductCategories createProductCategories(ProductCategories productCategories);
    ProductCategories getProductCategories(long id_category);
    ProductCategories updateProductCategories(ProductCategories productCategories,long id_category);
    void deleteProductCategories(long id_category);
}
