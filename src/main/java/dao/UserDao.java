package dao;

import model.User;

public interface UserDao {
    User createUser(User user);
    User getUser(long id_user);
    User updateUser(User user,long id_user);
    void deleteUser(long id_user);
}
