package dao;

import model.SkinType;

public interface SkinTypeDao {
    SkinType createSkinType(SkinType skinType);
    SkinType getSkinType(long id_skin);
    SkinType updateSkinType(SkinType skinType,long id_skin);
    void deleteSkinType(long id_skin);
}
