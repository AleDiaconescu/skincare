package dao;

import model.Shop;

public interface ShopDao {
    Shop createShop(Shop shop);
    Shop getShop(long id_shop);
    Shop updateShop(Shop shop,long id_shop);
    void deleteShop(long id_shop);
}
