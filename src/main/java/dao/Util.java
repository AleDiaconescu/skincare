package dao;

import model.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class Util {
    private static SessionFactory sessionFactory = createConfiguration().buildSessionFactory();

    private Util() {

    }

    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }


    public static Configuration createConfiguration() {
        Configuration config = new Configuration();
        Properties settings = new Properties();

        settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/skincare?serverTimezone=UTC");
        settings.put(Environment.USER, "alexandra.diaconescu28@yahoo.com");
        settings.put(Environment.PASS, "12345678");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");

        config.setProperties(settings);
        config.addAnnotatedClass(User.class);
        config.addAnnotatedClass(SkinType.class);
        config.addAnnotatedClass(SkincareProducts.class);
        config.addAnnotatedClass(Shop.class);
        config.addAnnotatedClass(ProductCategories.class);

        return config;
    }


}


