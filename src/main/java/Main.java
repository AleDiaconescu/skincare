import dao.*;
import model.*;

public class Main {
    public static void main(String[] args) {
        UserDao userDao=new UserDaoImplementation();
        User user1=new User("Ana");
        userDao.createUser(user1);

        SkinTypeDao skinTypeDao=new SkinTypeDaoImplementation();
//        SkinType skinType1=new SkinType("normal","without oily and dry skin");
//        skinTypeDao.createSkinType(skinType1);
//
//
//        SkincareProductsDao skincareProductsDao=new SkincareProductsDaoImplementation();
//        SkincareProducts skincareProduct1=new SkincareProducts("Clarins","moisturizing",25);
//        skincareProductsDao.createSkinCareProduct(skincareProduct1);
//
//        ProductCategoriesDao productCategoriesDao=new ProductCategoriesDaoImplementation();
//        ProductCategories productCategory1=new ProductCategories("moisturize");
//        productCategoriesDao.createProductCategories(productCategory1);
//
//        ShopDao shopDao = new ShopDaoImplementation();
//        Shop shop1=new Shop("Sephora","Alexandru Vaida Voevod",23,100);
//        shopDao.createShop(shop1);

        SkinType skinType2 = new SkinType("dry","without elasticity");
        User user2 = userDao.getUser(1);
        skinType2.setUser(user2);
        skinTypeDao.createSkinType(skinType2);




    }
}
