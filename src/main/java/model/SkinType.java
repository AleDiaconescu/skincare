package model;

import javax.persistence.*;

@Entity
@Table(schema = "skincare",name = "SkinType")
public class SkinType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "characteristics")
    private String characteristics;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    public User getUser() {
        return user;
    }

    public SkinType setUser(User user) {
        this.user = user;
        return this;
    }

    public SkinType() {

    }


    public String getName() {
        return name;
    }

    public SkinType setName(String name) {
        this.name = name;
        return this;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public SkinType setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
        return this;
    }

    public SkinType(String name, String characteristics) {
        this.name = name;
        this.characteristics = characteristics;
    }

    @ManyToOne
    @JoinColumn(name = "id_product")

    private SkincareProducts skincareProducts;

    public SkincareProducts getSkincareProducts() {
        return skincareProducts;
    }

    public SkinType setSkincareProducts(SkincareProducts skincareProducts) {
        this.skincareProducts = skincareProducts;
        return this;
    }
}