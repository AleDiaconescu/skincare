package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "skincare",name = "ProductCategories")
public class ProductCategories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "productCategories")
    List<SkincareProducts> skincareProductsList;

    public String getName() {
        return name;
    }

    public ProductCategories setName(String name) {
        this.name = name;
        return this;
    }

    public ProductCategories(String name) {
        this.name = name;
    }

    public ProductCategories(){

    }



}
