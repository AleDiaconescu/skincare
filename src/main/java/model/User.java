package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "skincare" , name="User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "user")
    List<SkinType>skinTypes;

    public List<SkinType>getSkinTypes(){
        return skinTypes;
    }
    public void setSkinTypes(List<SkinType>skinTypes){
        this.skinTypes=skinTypes;
    }

    public User(){

    }

    public User(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }
}
