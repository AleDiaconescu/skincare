package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "skincare", name = "Shop")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "adress")
    private String adress;

    @Column(name="stock")
    private int stock;

    @Column(name = "price")
    private int price;

    public String getName() {
        return name;
    }

    public Shop setName(String name) {
        this.name = name;
        return this;
    }

    public String getAdress() {
        return adress;
    }

    public Shop setAdress(String adress) {
        this.adress = adress;
        return this;
    }

    public int getStock() {
        return stock;
    }

    public Shop setStock(int stock) {
        this.stock = stock;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Shop setPrice(int price) {
        this.price = price;
        return this;
    }

    public Shop(String name, String adress, int stock,int price) {
        this.name = name;
        this.adress = adress;
        this.stock = stock;
        this.price=price;
    }

    public Shop(){

    }

    @ManyToMany
    @JoinTable(name = "Shop_SkincareProduct",joinColumns = @JoinColumn(name = "id_shop", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_product",referencedColumnName = "id"))

    private List<SkincareProducts>skincareProducts;
}
