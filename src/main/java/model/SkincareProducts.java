package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "skincare",name = "SkincareProducts")
public class SkincareProducts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name="name")
    private String name;

    @Column(name = "description")
    private String description;


    @Column(name="SPF")
    private int SPF;

    public String getName() {
        return name;
    }

    public SkincareProducts setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SkincareProducts setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getSPF() {
        return SPF;
    }

    public SkincareProducts setSPF(int SPF) {
        this.SPF = SPF;
        return this;
    }

    public SkincareProducts(String name, String description, int SPF) {
        this.name = name;
        this.description = description;

        this.SPF = SPF;
    }

    @ManyToOne
    @JoinColumn(name="id_category")

    private ProductCategories productCategories;

    public ProductCategories getProductCategories(){
        return productCategories;
    }

    @OneToMany
    @JoinColumn(name = "id_skin")
    List<SkinType>skinTypes;

    @ManyToOne
    @JoinColumn(name = "id_product")
    private SkincareProducts skincareProducts;


    @ManyToMany(mappedBy = "skincareProducts")
    private List<Shop>shops;

    public List<SkinType> getSkinTypes() {
        return skinTypes;
    }

    public SkincareProducts setSkinTypes(List<SkinType> skinTypes) {
        this.skinTypes = skinTypes;
        return this;
    }
}
